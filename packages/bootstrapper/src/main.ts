import path from 'path';
import { spawn } from 'child_process';

import logger from './logger';

async function launchPostgres() {
    const k8sApply = spawn('kubectl', ['apply', '-f', path.join('../', '../', '/3rdparty', 'postgres')])

    return new Promise((resolve, reject) => {
        k8sApply.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });

        k8sApply.stderr.on('data', (data) => {
            console.error(`stderr: ${data}`);
        });

        k8sApply.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
            if (code !== 0) {
                return reject('error applying postgress deployment');
            }
            return resolve(code);
        });
    })
}

async function launchThirdParty() {
    await launchPostgres();
}

async function launchServices () {

}

async function launchFederation() {

}


async function launchWeb() {

}

async function main() {
    logger.info('Starting bootstrapper...');

    await launchThirdParty();
}


main();
