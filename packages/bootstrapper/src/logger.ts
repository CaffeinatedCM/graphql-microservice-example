import winston from 'winston';
import { consoleFormat } from "winston-console-format";

const logLevels = {
    emerg: 0,
    alert: 1,
    crit: 2,
    error: 3,
    warning: 4,
    notice: 5,
    info: 6,
    debug: 7
}

const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.ms(),
        winston.format.errors({ stack: true }),
        winston.format.splat(),
        winston.format.json(),
    ),
    defaultMeta: { package: 'bootstrapper' },
    levels: logLevels,
    transports: [
        //
        // - Write all logs with level `error` and below to `error.log`
        // - Write all logs with level `info` and below to `combined.log`
        //
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' , level: 'info'}),
        new winston.transports.Console({ level: 'debug' ,
            format: winston.format.combine(
                winston.format.colorize({ all: true }),
                winston.format.padLevels({ levels: logLevels }),
                consoleFormat({
                    showMeta: true,
                    metaStrip: [ "service", "package"],
                    inspectOptions: {
                        depth: Infinity,
                        colors: true,
                        maxArrayLength: Infinity,
                        breakLength: 120,
                        compact: true,
                    },
                })
            ),
        }),
    ],
});

export default logger;
