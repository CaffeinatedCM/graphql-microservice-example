
import { actorsToSeed } from './actorData';

export const moviesToSeed = [
    {
        id: '15c28be4-7b38-436a-8923-9cf945d0c6a4',
        title: 'Star Wars: Episode IV - A New Hope',
        releaseDate: new Date('25 May 1977'),
        characters: [
            {
                name: 'Luke Skywalker',
                actor: actorsToSeed[0]
            },
            {
                name: 'Han Solo',
                actor: actorsToSeed[1]
            },
            {
                name: 'Darth Vader',
                actor: actorsToSeed[2]
            },
            {
                name: 'Princess Leia Organa',
                actor: actorsToSeed[3]
            }
        ]
    },
    {
        id: '671911ec-19e6-49c0-bc7f-c0362856baea',
        title: 'Star Wars: Episode V - The Empire Strikes Back',
        releaseDate: new Date('25 May 1977'),
        characters: [
            {
                name: 'Luke Skywalker',
                actor: actorsToSeed[0]
            },
            {
                name: 'Han Solo',
                actor: actorsToSeed[1]
            },
            {
                name: 'Darth Vader',
                actor: actorsToSeed[2]
            },
            {
                name: 'Princess Leia Organa',
                actor: actorsToSeed[3]
            }
        ]
    },
    {
        id: 'faaeb463-d2ef-4327-9831-d7463f723e41',
        title: 'Star Wars: Episode VI - Return of the Jedi',
        releaseDate: new Date('25 May 1977'),
        characters: [
            {
                name: 'Luke Skywalker',
                actor: actorsToSeed[0]
            },
            {
                name: 'Han Solo',
                actor: actorsToSeed[1]
            },
            {
                name: 'Darth Vader',
                actor: actorsToSeed[2]
            },
            {
                name: 'Princess Leia Organa',
                actor: actorsToSeed[3]
            }
        ]
    }
]