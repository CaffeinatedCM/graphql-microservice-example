export const actorsToSeed = [
    {
        id: 'dc6336c9-4322-4456-99eb-954daf5918e3',
        name: 'Mark Hamil',
        birthday: new Date('25 September 1951')
    },
    {
        id: '64199b33-20cc-4bf4-8e28-9bb3b245c45e',
        name: 'Harrison Ford',
        birthday: new Date('13 July 1942')
    },
    {
        id: 'a649dce7-8d09-4548-81eb-d7d2864a8f2c',
        name: 'David Prowse',
        birthday: new Date('01 July 1935'),
        deathDate: new Date('28 November 2020')
    },
    {
        id: '5a70d71b-4a7e-4765-8bef-9aaf6e7b6484',
        name: 'Carrie Fisher',
        birthday: new Date('21 October 1956'),
        deathDate: new Date('27 December 2016')
    }
]
