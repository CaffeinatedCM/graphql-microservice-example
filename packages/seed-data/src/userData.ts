import faker from 'faker';

const ids = `da2cdfc6-8fd5-431d-b213-1e8830e73c58
d2fa402e-2772-469b-95c6-0ee7b577d191
3eba90de-14be-4913-bffd-f8d5165baff7
933515f1-f302-4276-b54f-cd238b90fcd4
69f18f08-bc6e-40d1-a6f8-6269f6be0935
49646233-e583-4618-a213-7706253c476e
74cde4cc-cdfe-45fa-aa67-e0935e73eeab
d03311b4-134a-4a91-bd8b-8219a975a1be
46143985-c87c-4af4-8e15-e43929c2b1cd
741a9178-7e20-4e85-bd44-bcb36aa057e9
f1cbf6c6-e26b-4aca-a04a-69e927a7161e
870611c8-196c-4fb5-b385-2c21873ab81d
8ace753a-1760-4d44-958e-ad00520fcdfe
896807d1-1ba0-4000-ac55-5f9a892ab2ca
f8d71728-dc42-4bea-9237-f6cb7095d9a6
29a13804-d2b0-47ef-84d6-e416df6bc99f
01bdbf56-e957-4db9-9f62-27318f7f506e
41a8a52c-0851-4e2e-987e-01eb79bc86e2
377dd64c-f074-49ac-915c-6912094b6889
300fea7c-e844-4d3d-b4d2-2ed5567e34eb
2a648c24-8003-4b96-9378-1a5edb72203b
a7caa161-8e24-42ec-bc5f-c1fcb8c16ba8
82a3f5ad-639b-465d-b05a-7bed977100c8
a189c21c-a28e-4533-b249-5ccdceaf16f5
f9b4923d-448f-41f6-b1cd-4fb11e427ea0
b8953630-23f2-4521-884b-fb28e8051bb0
168760e0-ebd7-43b9-8354-dc4b82f87829
a6301c5c-573b-4de8-b17f-28f0507e7bfa
0bd1a06c-76ac-4b07-8f4b-31d1f4cb4d72
c3370576-4c6c-41c1-86dc-8bf06012f650`.split('\n');

export const usersToSeed = ids.map(id => ({
    id,
    username: faker.internet.userName(),
    birthday: faker.date.past(30),
    email: faker.internet.email(),
    password: faker.random.word()
}));