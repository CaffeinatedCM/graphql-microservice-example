import { gql } from 'apollo-server';
import { AuthenticationError, SchemaDirectiveVisitor } from 'apollo-server-express';
import { defaultFieldResolver } from 'graphql';

export default gql`
    # directive @loggedIn on FIELD_DEFINITION

    directive @auth on FIELD_DEFINITION
    
    interface Node {
        id: ID!
    }

    type PageInfo {
        hasNextPage: Boolean!
        hasPreviousPage: Boolean!
        startCursor: String
        endCursor: String
    }

    scalar Date
`;


class AuthDirective extends SchemaDirectiveVisitor {
    visitFieldDefinition(field) {
        //const requiredRole = this.args.requires;
        const originalResolve = field.resolve || defaultFieldResolver;
        field.resolve = function(...args) {
            const context = args[2];
            const user = context.user;
            const isAuthorized = !!user;
            if (!isAuthorized) {
                throw new AuthenticationError(`You need to be logged in!`);
            }
            return originalResolve.apply(this, args);
        }
    }
}
export {AuthDirective};
