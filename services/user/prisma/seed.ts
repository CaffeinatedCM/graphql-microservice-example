import { usersToSeed } from '@ccm-gql-example/seed-data';
import bcrypt from 'bcrypt';

import { PrismaClient } from './client';

const prisma = new PrismaClient();

async function main() {
    for (let user of usersToSeed) {
        await prisma.user.create({
            data: {
                id: user.id,
                username: user.username,
                email: user.email,
                birthday: user.birthday,
                password: await bcrypt.hash(user.password, 10)
            }
        })
    }
}

main()
    .catch((e) => {
        console.log('e', e)
        throw e
    })
    .finally(async () => {
        await prisma.$disconnect()
    })

