import { ApolloServer, gql, SchemaDirectiveVisitor } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import gqlCommon, { AuthDirective } from '@ccm-gql-example/gql-common';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { PrismaClient } from '../prisma/client';

const prisma = new PrismaClient();

const typeDefs = gql`
    ${gqlCommon}
    
    type Query {
        user(id: ID!): User
        me: User @auth
    }
    
    type Mutation {
        echo(str: String!): String!
        login(credentials: LoginInput): AuthPayload
        register(details: RegisterInput): AuthPayload
    }
    
    type AuthPayload {
        token: String
        user: User
    }
    
    type User @key(fields: "id") {
        id: ID! @auth
        username: String!
        email: String! @auth
        birthday: Date!
    }
    
    input LoginInput {
        username: String!
        password: String!
    }
    
    input RegisterInput {
        username: String!
        password: String!
        email: String!
        birthday: Date!
    }
`;

const resolvers = {
    Query: {
        user(parent, args, context, info) {
            return prisma.user.findUnique({
                where: {
                    id: args.id,
                }
            })
        },
        me(parent, args, context) {
            return prisma.user.findUnique({
                where: {
                    id: context.user?.userId
                }
            })
        }
    },
    Mutation: {
      echo(_, args, context) {
          return `${context.user?.username ?? 'Unknown'}: ${args.str}`;
      },
      async login(_, args, context, info) {
          const { username, password } = args.credentials;

          const user = await prisma.user.findUnique({
              where: {
                  username,
              }
          });
          if (!user) {
              throw new Error('Invalid password');
          }

          const validPwd = await bcrypt.compare(password, user.password)
          if (!validPwd) {
              throw new Error('Invalid password');
          }

          const token = jwt.sign({ userId: user.id, username: user.username }, "SECRET", {
              subject: user.id,
              issuer: 'user_service',
          });

          context.user = getUserDetails(null, token);

          return {
              token,
              user,
          };
      },
      async register(_, args, context, info) {

          const { username, password, email, birthday } = args.details;

          const pwdHash = await bcrypt.hash(password, 10);

          const user = await prisma.user.create({
              data: {
                  username,
                  email,
                  birthday: new Date(birthday),
                  password: pwdHash
              }
          });

          const token = jwt.sign({ userId: user.id, username: user.username }, "SECRET", {
              subject: user.id,
              issuer: 'user_service',
          });

          context.user = getUserDetails(null, token);

          return {
              token,
              user,
          }
      }
    },
    User: {
      async __resolveReference(user) {
          return prisma.user.findUnique({
              where: {
                  id: user.id
              }
          });
      }
    }
};

function getTokenPayload(token) {
    return jwt.verify(token, "SECRET");
}

function getUserDetails(req, authToken) {
    if (req) {
        const authHeader = req.headers.authorization;
        if (authHeader) {
            const token = authHeader.replace('Bearer ', '');
            if (!token) {
                throw new Error('Not authenticated');
            }
            return getTokenPayload(token);
        }
    } else if (authToken) {
        return getTokenPayload(authToken);
    }

    throw new Error('Not Authenticated');
}

const schema = buildFederatedSchema([{ typeDefs, resolvers }]);
const directives = { auth: AuthDirective };
SchemaDirectiveVisitor.visitSchemaDirectives(schema, directives);

const server = new ApolloServer({
    schema,
    context: ({ req }) => {
        const context: Record<string, any> = {};
        try {
            context.user = getUserDetails(req, null);
        } catch {
            console.log('Not authenticated request')
        }
        return context;
    },
});

server.listen(4001).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});
