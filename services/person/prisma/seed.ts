import { actorsToSeed } from '@ccm-gql-example/seed-data';

import { PrismaClient } from './client';

const prisma = new PrismaClient();

async function main() {
    for (let person of actorsToSeed) {
        await prisma.person.create({
            data: {
                id: person.id,
                name: person.name,
                birthday: person.birthday,
                deathDate: person.deathDate,
            }
        })
    }
}

main()
    .catch((e) => {
        throw e
    })
    .finally(async () => {
        await prisma.$disconnect()
    })
