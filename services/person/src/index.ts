import { ApolloServer, gql } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import gqlCommon from '@ccm-gql-example/gql-common';

import { PrismaClient } from '../prisma/client';

const prisma = new PrismaClient();

const typeDefs = gql`
    ${gqlCommon}
    
    type Query {
        person(id: ID!): Person
        allPeople: PeopleConnection!
    }
    
    type PeopleConnection {
        pageInfo: PageInfo!
        edges: [PeopleEdge]
        people: [Person]
    }
    
    type PeopleEdge {
        cursor: String!
        node: Person!
    }
    
    type Person implements Node @key(fields: "id") {
        id: ID!
        name: String!
        birthday: Date!
        deathDate: Date
    }
`;

const resolvers = {
    Query: {
        async person(parent, args) {
            const person = await prisma.person.findUnique({
                where: {
                    id: args.id
                }
            });
            return person;
        },
        async allPeople(parent, args, context, info) {
            const people = await prisma.person.findMany({});
            return {
                pageInfo: {
                    hasNext: false,
                    hasPrevious: false
                },
                edges: people.map(person => ({
                    cursor: person.id,
                    node: person
                })),
                people: people
            }
        }
    },
    Person: {
      async __resolveReference(person) {
          const personFull = await prisma.person.findUnique({
              where: {
                  id: person.id
              }
          });
          return personFull;
      }
    }
};

const server = new ApolloServer({
    schema: buildFederatedSchema([{ typeDefs, resolvers }]),
});

server.listen(4003).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});
