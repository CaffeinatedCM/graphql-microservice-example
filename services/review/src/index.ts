import { ApolloServer, gql } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import gqlCommon from '@ccm-gql-example/gql-common';

import {Prisma, PrismaClient} from '../prisma/client';
import ReviewWhereInput = Prisma.ReviewWhereInput;

const prisma = new PrismaClient();

const typeDefs = gql`
    ${gqlCommon}

    type Query {
        fiveStars: Review
        review(id: ID!): Review
        allReviews(first: Int, after: String, last: Int, before: String, filter: ReviewFilterInput): ReviewsConnection!
        allMoviesByRating(direction: String!): MoviesConnection!
    }
    
    input ReviewFilterInput {
        movieId: ID
        reviewerId: ID
    }
    
    type ReviewsConnection {
        pageInfo: PageInfo!
        edges: [ReviewEdge]
        reviews: [Review]
    }

    type MoviesConnection {
        pageInfo: PageInfo!
        edges: [MovieEdge]
        movies: [Movie]
    }
    
    type ReviewEdge {
        cursor: String!
        node: Review!
    }
    
    type MovieEdge {
        cursor: String!
        node: Movie!
    }
    
    type Review implements Node @key(fields: "id") {
        id: ID!
        rating: Int!
        title: String
        body: String
        reviewer: User!
        movie: Movie!
    }
    
    extend type Movie implements Node @key(fields: "id") {
        id: ID! @external
        rating: Float
    }
    
    extend type User implements Node @key(fields: "id") {
        id: ID! @external
        reviews: ReviewsConnection!
    }
`;

const resolvers  = {
    Query: {
        async review(parent, args, context, info) {
            const review = await prisma.review.findUnique({
                where: { id: args.id },
            });
            return review;
        },
        async allReviews(_, args) {
            let reviews = [];
            let hasNextPage = false, hasPreviousPage = false;
            const where: ReviewWhereInput = {
                movieId: args.filter?.movieId,
                reviewerId: args.filter?.reviewerId,
            }
            if (args.after) {
                reviews = await prisma.review.findMany({
                    include: {
                        movie: true,
                        reviewer: true,
                    },
                    orderBy: {
                        createdAt: 'asc'
                    },
                    cursor: {
                        createdAt: new Date(args.after * 1),
                    },
                    where,
                    skip: 1,
                    take: args.first + 1
                });
                hasNextPage = reviews.length > args.first;
                reviews = reviews.slice(0, reviews.length - 1)
            } else if (args.before) {
                reviews = await prisma.review.findMany({
                    include: {
                        movie: true,
                        reviewer: true,
                    },
                    orderBy: {
                        createdAt: 'asc'
                    },
                    where,
                    cursor: {
                        createdAt: new Date(args.before * 1),
                    },
                    skip: 1,
                    take: -args.last - 1
                });
                hasPreviousPage = reviews.length > args.last;
                reviews = reviews.slice(1)
            } else {
                reviews = await prisma.review.findMany({
                    include: {
                        movie: true,
                        reviewer: true,
                    },
                    where,
                    orderBy: {
                        createdAt: 'asc'
                    },
                    take: (args.first ?? 10) + 1
                });
                hasNextPage = reviews.length > (args.first ?? 10);
                reviews = reviews.slice(0, reviews.length - 1)
            }
            return {
                pageInfo: {
                    hasNextPage,
                    hasPreviousPage,
                    startCursor: reviews[0].createdAt,
                    endCursor: reviews[reviews.length - 1].createdAt
                },
                edges: reviews.map(review => ({
                    cursor: review.createdAt,
                    node: review
                })),
                reviews,
            }
        },
        async allMoviesByRating(parent, args, context, info) {
            const movieRatings = await prisma.review.groupBy({
                by: ['movieId'],
                avg: {
                    rating: true
                },
                orderBy: {
                    _avg: {
                        rating: args.direction
                    }
                }
            });
            return {
                pageInfo: {
                    hasNextPage: false,
                    hasPreviousPage: false,
                },
                edges: movieRatings.map(({ movieId, avg: rating}) => ({
                    cursor: movieId,
                    node: {
                        id: movieId,
                        rating
                    }
                })),
                movies: movieRatings.map(({ movieId, avg: rating}) => ({
                    id: movieId,
                    rating,
                }))
            }
        }
    },
    User: {
        async reviews(user) {
            const userReviews = await prisma.review.findMany({
                where: {
                    reviewer: {
                        id: user.id
                    }
                },
                include: {
                    movie: true,
                    reviewer: true
                }
            });
            return  {
                pageInfo: {
                    hasNextPage: false,
                    hasPreviousPage: false,
                },
                edges: userReviews.map(review => ({
                    cursor: review.id,
                    node: review
                })),
                reviews: userReviews,
            }
        },
        async __resolveReference(user) {
            const userReviews = await prisma.review.findMany({
                where: {
                    reviewer: {
                        id: user.id
                    }
                },
                include: {
                    movie: true,
                    reviewer: true
                }
            });
            return {
                ...user,
                reviews: {
                    pageInfo: {
                        hasNextPage: false,
                        hasPreviousPage: false,
                    },
                    edges: userReviews.map(review => ({
                        cursor: review.id,
                        node: review
                    })),
                    reviews: userReviews,
                }
            }
        }
    },
    Review: {
      async __resolveReference(review) {
          const resolvedReview = await prisma.review.findUnique({
              where: { id: review.id },
          });
          return resolvedReview;
      }
    },
    Movie: {
        async rating(movie) {
            const rating =  await prisma.review.aggregate({
                avg: {
                    rating: true
                },
                where: {
                    movie: {
                        id: movie.id
                    }
                }
            });
            return rating.avg.rating;
        },
        async __resolveReference(movie) {
            const rating =  await prisma.review.aggregate({
                avg: {
                    rating: true
                },
                where: {
                    movie: {
                        id: movie.id
                    }
                }
            });
            return { ...movie, rating: rating.avg.rating}
        }
    }
};

const server = new ApolloServer({
    schema: buildFederatedSchema([{ typeDefs, resolvers }])
});

server.listen(4004).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});
