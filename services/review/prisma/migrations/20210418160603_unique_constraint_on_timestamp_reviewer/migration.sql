/*
  Warnings:

  - A unique constraint covering the columns `[createdAt,reviewerId]` on the table `Review` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Review.createdAt_reviewerId_unique" ON "Review"("createdAt", "reviewerId");
