/*
  Warnings:

  - A unique constraint covering the columns `[createdAt]` on the table `Review` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "Review.createdAt_reviewerId_unique";

-- CreateIndex
CREATE UNIQUE INDEX "Review.createdAt_unique" ON "Review"("createdAt");
