import { reviewsToSeed } from '@ccm-gql-example/seed-data';

import { PrismaClient } from './client';

const prisma = new PrismaClient();

async function main() {
    for (let review of reviewsToSeed) {
        await prisma.review.create({
            data: {
                id: review.id,
                rating: review.rating,
                title: review.title,
                body: review.body,
                movie: {
                    connectOrCreate: {
                        create: {
                            id: review.movieId
                        },
                        where: { id: review.movieId }
                    }
                },
                reviewer: {
                    connectOrCreate: {
                        create: {
                            id: review.reviewerId,
                        },
                        where: { id: review.reviewerId }
                    }
                }
            }
        })
    }
}

main()
    .catch((e) => {
        console.log('e', e)
        throw e
    })
    .finally(async () => {
        await prisma.$disconnect()
    })

