import { ApolloServer, gql } from 'apollo-server';
import { buildFederatedSchema } from '@apollo/federation';
import gqlCommon from '@ccm-gql-example/gql-common';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { PrismaClient } from '../prisma/client';

const prisma = new PrismaClient()

const typeDefs = gql`
    ${gqlCommon}
    
    type Query {
        movie(id: ID!): Movie
        allMovies: MoviesConnection!
    }
    
    type Mutation {
        movieEcho(str: String!): String!
    }

    type MoviesConnection {
        pageInfo: PageInfo!
        edges: [MovieEdge]
        movies: [Movie]
    }
    
    type MovieEdge {
        cursor: String!
        node: Movie!
    }
    
    type Movie implements Node @key(fields: "id") {
        id: ID!
        title: String!
        releaseDate: Date!
        charactersConnection(first: Int): MovieCharacterConnection!
    }
    
    type MovieCharacterConnection {
        pageInfo: PageInfo!
        edges: [MovieCharacterEdge]
        characters: [Character]
    }
    
    type MovieCharacterEdge {
        cursor: String!
        node: Character!
    }
    
    type Character {
        id: ID!
        name: String!
        person: Person! @provides(fields: "name")
    }
    
    extend type Person implements Node @key(fields: "id") {
        id: ID! @external
        name: String! @external
    }
`;

const resolvers = {
    Query: {
        async movie(parent, args, context, info) {
            const movie = await prisma.movie.findFirst({
                where: {
                    id: args.id,
                },
                select: {
                    id: true,
                    characters: true,
                    createdAt: true,
                    updatedAt: true,
                    releaseDate: true,
                    title: true
                }
            })
            return movie;
        },
        async allMovies(parent, args, context, info) {
            const allMovies =  await prisma.movie.findMany({
                select: {
                    id: true,
                    characters: true,
                    createdAt: true,
                    updatedAt: true,
                    releaseDate: true,
                    title: true
                }
            });
            return {
                pageInfo: {
                    hasNext: false,
                    hasPrevious: false,
                },
                edges: allMovies.map(movie => {
                    return {
                        cursor: movie.id,
                        node: movie
                    }
                }),
                movies: allMovies
            }
        }
    },
    Movie: {
        async charactersConnection(movie, args) {
            const chars = (await prisma.character.findMany({
                where: {
                    movies: {
                        some: {
                            id: movie.id
                        }
                    }
                },
                select: {
                    id: true,
                    name: true,
                    actors: true
                }
            })).map(x => {
                return {
                    ...x,
                    person: x.actors[0]
                }
            });
            return {
                pageInfo: {
                    hasNext: false,
                    hasPrevious: false,
                },
                edges: chars.map(char => {
                    return {
                        cursor: char.id,
                        node: char
                    }
                }),
                characters: chars
            }
        },
        async __resolveReference(movie) {
            const resolvedMovie = await prisma.movie.findUnique({
                where: {
                    id: movie.id,
                },
            })
            return resolvedMovie;
        }
    },
    Person: {
        async name(person, args) {
            if (person.name) {
                return person.name;
            }
            const actor = await prisma.actorStub.findFirst({ where: { id: person.id }})
            return actor.name;
        },
        __resolveReference(person) {
            return { id: person.id }
        }
    },
    Mutation: {
        movieEcho(_, args, context) {
            return `${context.user?.username ?? 'Unknown'}: ${args.str}`;
        },
    },
}

function getTokenPayload(token) {
    return jwt.verify(token, "SECRET");
}

function getUserDetails(req, authToken) {
    if (req) {
        const authHeader = req.headers.authorization;
        if (authHeader) {
            const token = authHeader.replace('Bearer ', '');
            if (!token) {
                throw new Error('Not authenticated');
            }
            return getTokenPayload(token);
        }
    } else if (authToken) {
        return getTokenPayload(authToken);
    }

    throw new Error('Not Authenticated');
}

const server = new ApolloServer({
    schema: buildFederatedSchema([{ typeDefs, resolvers }]),
    context: ({ req }) => {
        const context: Record<string, any> = {};
        try {
            context.user = getUserDetails(req, null);
        } catch {
            console.log('Not authenticated request')
        }
        return context;
    }
});

server.listen(4002).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});
