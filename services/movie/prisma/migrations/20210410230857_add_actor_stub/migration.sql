-- CreateTable
CREATE TABLE "ActorStub" (
    "id" UUID NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_ActorStubToCharacter" (
    "A" UUID NOT NULL,
    "B" UUID NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_ActorStubToCharacter_AB_unique" ON "_ActorStubToCharacter"("A", "B");

-- CreateIndex
CREATE INDEX "_ActorStubToCharacter_B_index" ON "_ActorStubToCharacter"("B");

-- AddForeignKey
ALTER TABLE "_ActorStubToCharacter" ADD FOREIGN KEY ("A") REFERENCES "ActorStub"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ActorStubToCharacter" ADD FOREIGN KEY ("B") REFERENCES "Character"("id") ON DELETE CASCADE ON UPDATE CASCADE;
