/*
  Warnings:

  - A unique constraint covering the columns `[name]` on the table `Character` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[title]` on the table `Movie` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Character.name_unique" ON "Character"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Movie.title_unique" ON "Movie"("title");
