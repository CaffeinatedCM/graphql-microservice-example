/*
  Warnings:

  - A unique constraint covering the columns `[name]` on the table `ActorStub` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `name` to the `ActorStub` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ActorStub" ADD COLUMN     "name" VARCHAR(255) NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "ActorStub.name_unique" ON "ActorStub"("name");
