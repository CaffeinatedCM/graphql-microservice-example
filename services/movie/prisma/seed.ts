import { moviesToSeed } from '@ccm-gql-example/seed-data';

import { PrismaClient } from './client';


const prisma = new PrismaClient()

async function main() {
    for (let movie of moviesToSeed) {
        await prisma.movie.create({
            data: {
                id: movie.id,
                title: movie.title,
                releaseDate: movie.releaseDate,
                characters: {
                    connectOrCreate: movie.characters.map(character => ({
                            create: {
                                name: character.name,
                                actors: {
                                    connectOrCreate: [{
                                        create: {
                                            id: character.actor.id,
                                            name: character.actor.name
                                        },
                                        where: { name: character.actor.name }
                                    }]
                                }
                            },
                            where: { name: character.name }

                    }))
                }
            }
        })
    }
}

main()
    .catch((e) => {
        throw e
    })
    .finally(async () => {
        await prisma.$disconnect()
    })