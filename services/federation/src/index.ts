import { ApolloServer } from 'apollo-server';
import {ApolloGateway, RemoteGraphQLDataSource} from '@apollo/gateway';

class RemoteService extends RemoteGraphQLDataSource {
    private name: string;

    constructor(config) {
        super(config);
        this.name = config.name;
    }

    willSendRequest({ request, context}) {
        request.http.headers.set('authorization', context.auth);
    }

    didReceiveResponse({ response, request, context }) {
        if (!context.auth && this.name === 'user' ) {
            const token = response?.data?.login?.token || response?.data?.register?.token;

            if (token) {
                context.auth = `Bearer ${token}`
            }
        }

        return response;
    }
}

const gateway = new ApolloGateway({
    logger: console,
    serviceList: [
        { name: 'user', url: 'http://localhost:4001'},
        { name: 'movie', url: 'http://localhost:4002'},
        { name: 'person', url: 'http://localhost:4003' },
        { name: 'review', url: 'http://localhost:4004'}
    ],
    debug: true,
    buildService({ name, url }) {
        const service = new RemoteService( { name, url })
        return service;
    },
});

const server = new ApolloServer({
    gateway,
    subscriptions: false,
    tracing: true,
    context: ({ req }) => {
        const context : Record<string, any> = {};

        context.auth = req.headers['authorization'];

        return context;
    }

});

server.listen(4000).then(({url }) => {
    console.log(`🚀 Server ready at ${url}`);
});
