import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {CircularProgress, colors, Container, createMuiTheme, CssBaseline, MuiThemeProvider} from '@material-ui/core';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ApolloProvider } from '@apollo/client/react';

import {AppBar} from './components/AppBar/AppBar';
const Home = React.lazy(() => import('./screens/Home'))
const MovieDetails = React.lazy(() => import('./screens/MovieDetails'))
const MovieList = React.lazy(() => import('./screens/MovieList'))
const UserProfile = React.lazy(() => import('./screens/UserProfile'))
const PersonProfile = React.lazy(() => import('./screens/PersonProfile'))

const client = new ApolloClient({
    uri: '/graphql',
    cache: new InMemoryCache()
});

export default function BasicExample() {
    const theme = createMuiTheme({
        palette: {
            primary: colors.blue,
            secondary: colors.green
        }
    });

    return (
        <ApolloProvider client={client}>
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <Router>
                    <AppBar />
                    <Container>
                        <React.Suspense fallback={<CircularProgress />}>
                            <Switch>
                                <Route exact path="/">
                                    <Home />
                                </Route>
                                <Route exact path="/movies">
                                    <MovieList />
                                </Route>
                                <Route path="/movies/:id">
                                    <MovieDetails />
                                </Route>
                                <Route path="/user/:id">
                                    <UserProfile />
                                </Route>
                                <Route path="/person/:id">
                                    <PersonProfile />
                                </Route>
                            </Switch>
                        </React.Suspense>
                    </Container>
                </Router>
            </MuiThemeProvider>
        </ApolloProvider>
    );
}
