import React from 'react';
import { Typography, List, ListItem, ListItemText, ListItemIcon } from '@material-ui/core';
import * as Icons from '@material-ui/icons';
import { useParams } from 'react-router-dom';
import { useQuery, gql } from '@apollo/client';

import ReviewCard from '../components/ReviewCard';

const USER_QUERY = gql`
    query UserProfile($userId: ID!) {
        user(id: $userId) {
            username
            email
            birthday
        }
        allReviews(filter: { reviewerId: $userId}) {
            reviews {
                id
                title
                body
                rating
                reviewer {
                    id
                    username
                }
                movie {
                    id
                    title
                }
            }
        }
    }
`

function UserProfile() {
    const { id: userId  } = useParams<{ id: string }>();
    const { data } = useQuery(USER_QUERY, {
        variables: {
            userId,
        }
    })

    return (
        <div>
            <Typography variant="h1">User: @{data?.user?.username}</Typography>
            <section>
                <Typography variant="h2">Bio</Typography>
                <List>
                    <ListItem>
                        <ListItemIcon>
                            <Icons.AccountCircle />
                        </ListItemIcon>
                        <ListItemText>
                            Username: @{data?.user?.username}
                        </ListItemText>
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <Icons.Email />
                        </ListItemIcon>
                        <ListItemText>
                            Email: {data?.user?.email}
                        </ListItemText>
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <Icons.Today />
                        </ListItemIcon>
                        <ListItemText>
                            Birthday: {data?.user?.birthday ? (new Date(data?.user?.birthday)).toDateString() : ''}
                        </ListItemText>
                    </ListItem>
                </List>
            </section>
            <section>
                <Typography variant="h2">Reviews</Typography>
                {data?.allReviews?.reviews.map(review => (
                    <ReviewCard hideUserButton review={review} key={review.id} />
                ))}
            </section>
        </div>
    );
}

export default UserProfile;