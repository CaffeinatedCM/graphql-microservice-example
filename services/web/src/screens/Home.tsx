import React from 'react';
import { Typography } from '@material-ui/core';
import { useQuery, gql } from '@apollo/client';

import MovieCard from '../components/MovieCard';
import ReviewCard from '../components/ReviewCard';

const HOME_QUERY = gql`
    query {
        topMovies: allMoviesByRating(direction: "desc") {
            movies {
                id
                title
                rating
            }
        }
        recentReviews: allReviews(first: 5) {
            reviews {
                id
                title
                body
                rating
                reviewer {
                    id
                    username
                }
                movie {
                    id
                    title
                }
            }
        }
    }
`

function Home() {
    const { data } = useQuery(HOME_QUERY);
    return (
        <div>
            <Typography variant={"h1"}>
                Home
            </Typography>
            <section>
                <Typography variant="h2">
                    Top Movies
                </Typography>
                {data?.topMovies?.movies.map(movie => (
                    <MovieCard key={movie.id} movie={movie} />
                ))}
            </section>
            <section>
                <Typography variant="h2">
                    Recent Reviews
                </Typography>
                {data?.recentReviews?.reviews.map(review => (
                    <ReviewCard key={review.id} review={review} />
                ))}
            </section>
        </div>
    );
}

export default Home;