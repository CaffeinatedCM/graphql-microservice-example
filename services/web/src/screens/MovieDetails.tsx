import React from 'react';
import { Typography } from '@material-ui/core';
import { useQuery, gql } from '@apollo/client';
import { useParams } from 'react-router-dom';
import { Rating } from '@material-ui/lab';

import MovieCard from '../components/MovieCard';
import ReviewCard from '../components/ReviewCard';

const MOVIE_DETAILS_QUERY = gql`
    query MovieDetails($movieId: ID!){
        movie(id: $movieId) {
            id
            title
            releaseDate
            rating
            charactersConnection {
                characters {
                    id
                    name
                    person {
                        name
                    }
                }
            }
        }
        reviews: allReviews(first: 30, filter: { movieId: $movieId }) {
            reviews {
                id
                title
                body
                rating
                reviewer {
                    id
                    username
                }
                movie {
                    id
                    title
                }
            }
        }
    }
`

function MovieDetails() {
    const { id: movieId } = useParams<{ id: string }>();
    const { data } = useQuery(MOVIE_DETAILS_QUERY, {
        variables: {
            movieId,
        }
    })
    return (
        <div>
            <header>
                <Typography variant="h2" component="h1">{data?.movie?.title}</Typography>
                <Typography variant="subtitle1">{(new Date(data?.movie?.releaseDate ?? 0)).toDateString()}</Typography>
                <Rating readOnly value={Math.floor(data?.movie?.rating)} max={10}  />
            </header>
            <section>
                <Typography variant="h3" component="h2">Characters</Typography>
                <ul>
                    {data?.movie?.charactersConnection?.characters.map(char => (
                        <li key={char.id}>
                            <Typography variant="body1">
                                {char.name} - {char.person.name}
                            </Typography>
                        </li>
                    ))}
                </ul>
            </section>

            <section>
                <Typography variant="h3" component="h2">Reviews</Typography>
                {data?.reviews?.reviews.map(review => (
                    <ReviewCard key={review.id} review={review} hideMovieButton />
                ))}
            </section>
        </div>
    );
}

export default MovieDetails;