import React from 'react';
import { Typography } from '@material-ui/core';
import { useQuery, gql } from '@apollo/client';

import MovieCard from '../components/MovieCard';

const MOVIE_LIST = gql`
    query {
        allMovies {
            movies {
                id
                title
                rating
            }
        }
    }
`

function MovieList() {
    const { data } = useQuery(MOVIE_LIST);

    return (
        <main>
            <Typography variant="h1">Movie List</Typography>
            <section>
                {data?.allMovies?.movies.map(movie => <MovieCard movie={movie} key={movie.id} /> )}
            </section>
        </main>
    );
}

export default MovieList;