import React, {useState} from 'react';
import {AppBar as MuiAppBar, Toolbar, IconButton, Typography, Button, Drawer, ListItemProps, ListItem, List, ListItemIcon, ListItemText} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Menu as MenuIcon, Home as HomeIcon, Movie as MovieIcon, Explore as ExploreIcon } from '@material-ui/icons';
import { NavLink, Link } from 'react-router-dom';
import clsx from 'clsx'

interface ListItemLinkProps extends ListItemProps<'a', {button?: true}>{
    to: string;
}

const MyNavLink = (to) => React.forwardRef(({ className, children }, ref) => {
    const classes = useStyles();

    return (
        <NavLink exact ref={ref} to={to} className={clsx(className, classes.myNavLink)}>{children}</NavLink>
    );
})

function ListItemLink({to, ...props} : ListItemLinkProps) {
    return <ListItem button component={MyNavLink(to)} {...props} />;
}

export function AppBar() {
    const classes = useStyles();
    const [open, setOpen] = useState(false);

    return (
        <>
            <MuiAppBar position="sticky">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={() => setOpen(open => !open)}>
                        <MenuIcon />
                    </IconButton>
                    <div className={classes.title}>
                        <Button color="inherit" href="/">
                            <Typography variant="h6" className={classes.title}>
                                BDMI
                            </Typography>
                        </Button>
                    </div>
                    <Button color="inherit">Login</Button>
                </Toolbar>
            </MuiAppBar>
            <Drawer anchor="left" variant="temporary" onClose={() => setOpen(false)} open={open}>
                <div className={classes.drawerRoot}>
                    <List component="nav" aria-label="main mailbox folders">
                        <ListItemLink to="/">
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primary="Home" />
                        </ListItemLink>
                        <ListItemLink to="/movies">
                            <ListItemIcon>
                                <MovieIcon />
                            </ListItemIcon>
                            <ListItemText primary="Movies" />
                        </ListItemLink>
                        <ListItem component="a" button href="/graphql">
                            <ListItemIcon>
                                <ExploreIcon />
                            </ListItemIcon>
                            <ListItemText primary="GraphQL Playground" />
                        </ListItem>
                    </List>
                </div>
            </Drawer>
        </>
    )
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
        drawerRoot: {
            width: 250,
        },
        myNavLink: {
            '&.active': {
                color: theme.palette.primary.contrastText,
                backgroundColor: theme.palette.primary.light
            }
        }
    }),
);