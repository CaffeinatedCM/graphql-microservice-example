import React from 'react';
import {Card, CardHeader, CardActions, Button, CardContent, Typography, makeStyles, createStyles} from '@material-ui/core';

const ReviewCard = ({ review, hideUserButton = false, hideMovieButton = false}) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardHeader title={review.title} subheader={`${review.movie.title} | ${Math.floor(review.rating)}/10`} />
            <CardContent>
                <Typography variant="body1">
                    {review.body}
                </Typography>
                <br />
                <Typography variant="body2">
                    {review.reviewer.username ? `@${review.reviewer.username}` : null}
                </Typography>
            </CardContent>
            <CardActions>
                {
                    !hideUserButton && (
                        <Button variant="contained" color="secondary" href={`/user/${review.reviewer.id}`}>
                        View User
                    </Button>
                    )
                }
                {
                    !hideMovieButton && (
                        <Button variant="outlined" color="primary" href={`/movies/${review.movie.id}`}>
                            View Movie Details
                        </Button>
                    )
                }
            </CardActions>
        </Card>
    )
}

const useStyles = makeStyles(theme => createStyles({
    root: {
        margin: theme.spacing(2)
    }
}))

export default ReviewCard;
