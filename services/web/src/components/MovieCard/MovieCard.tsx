import React from 'react';
import { Button, Card, CardHeader, CardContent, CardActions, Typography, makeStyles, createStyles} from '@material-ui/core';

const MovieCard = ({ movie }) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardHeader title={movie.title} subheader={`Rating: ${Math.floor(movie.rating)}/10`} />
            <CardActions>
                <Button variant="contained" color="secondary" href={`/movies/${movie.id}`}>
                    View Movie
                </Button>
            </CardActions>
        </Card>
    )
}

const useStyles = makeStyles(theme => createStyles({
    root: {
        margin: theme.spacing(2)
    }
}))

export default MovieCard;
