import React from "react";
import ReactDOM from "react-dom";
import { CircularProgress } from '@material-ui/core';

import App from './App';

ReactDOM.render(
    <React.StrictMode>
        <React.Suspense fallback={<CircularProgress />}>
            <App />
        </React.Suspense>
    </React.StrictMode>,
    document.getElementById("root")
);
