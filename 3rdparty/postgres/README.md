stores postgres deployment files

update `postgres-storage.yaml` as necessary for your environment

create the following databases
- users
- reviews
- movies
- people

```
kubectl exec {{pod}} -it psql -U postgresadmin -d postgresdb
```

```psql
create database users;
create database  reviews;
create database  movies;
create database  people;
```
